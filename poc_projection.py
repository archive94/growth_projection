#!/usr/bin/env python3

import random

#argparse - number cycles, growth rate, starting value
#def one_cycle():

GROWTH_RATE=0.03
STDDEV=0.01
STARTING_SIZE=1.00

size = STARTING_SIZE

for _ in range(10000):
  m_growth = random.normalvariate( GROWTH_RATE, STDDEV )
  size = size * ( 1 + m_growth )
  
print( size )