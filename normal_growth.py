#!/usr/bin/env python3

import random
import argparse

#argparse - number cycles, growth rate, starting value
#def one_cycle():

def main():

  version = '2018/08/04'
  parser = argparse.ArgumentParser( description='Growth Projector.  Args are projection paramenters' )
  parser.add_argument( '--cycles', '-c', metavar='NUMBER', default=1000, help='Number of cycles to run each projection' )
  parser.add_argument( '--proj', '-p', metavar='NUMBER', default=12, help='Number of steps in each projection' )
  parser.add_argument( '--size', '-S', metavar='VALUE', default=1, help='The staring value for the growth projection' )
  parser.add_argument( '--growth-rate', '-g', metavar='PERCENTAGE', default=0.01, help='Growth rate each cycle' )
  parser.add_argument( '--sigma', '-s', metavar='STDDEV', default=0.01, help='Growth std deviation each cycle' )
  parser.add_argument( '--verbose', '-v', action='store_true', default=False, help='Verbose output' )
  parser.add_argument( '--version', '-V', action='store_true', default=False, help='Print version and exit' )
  args = parser.parse_args()
  
  if args.version:
    print_version( version )
  
  if not args.cycles.isdigit():
    nondigit_exit( "number of cycles" )
  if not args.proj.isdigit():
    nondigit_exit( "number of projections" )
  if is_not_number( args.growth_rate ):
    nondigit_exit( "growth rate" )
  if is_not_number( args.sigma ):
    nondigit_exit( "standard deviation" )
  if is_not_number( args.size ):
    nondigit_exit( "initial size" )

  size = float(args.size)
  g_rate = float(args.growth_rate)
  g_sig = float(args.sigma)
  proj = int(args.proj)
  cycles = int(args.cycles)
  
  proj_results = []
  
  for _ in range( proj ):
    proj_result = projection(size, g_rate, g_sig, cycles)
    proj_results.append( proj_result )
    if args.verbose:
      print( "Proj: {val}\n  Growth: {res}".format(val=_, res=proj_result) )

  print(
  "Results:\
  \n Start: {}\
  \n Mean End: {}\
  \n Std. Dev: {}"
  .format(size, mean(proj_results), std_dev(proj_results))
  )

def std_dev( vals ):
  avg = mean( vals )
  sum = 0
  for _ in vals:
    sum += (_-avg)**2
  stddev = ( sum / len(vals) )**0.5
  return stddev

def mean( vals ):
  total = 0
  count = 0
  for _ in vals:
    total += _
    count += 1
  return total/count
    

def projection( size, grate, gsig, cycles ):
  for _ in range( cycles ):
    size = cycle( size, grate, gsig )
  return size

def cycle( size, grate, gsig ):
  cycle_growth = random.normalvariate( grate, gsig )
  new_size = size * ( 1 + cycle_growth )
  return new_size

def nondigit_exit( name ):
  print(
  "The value provided for {} is not a digit.\
  \nPlease provide a digit next time.\
  \nExiting . . .".format( name )
  )
  exit( 1 )

def is_not_number( val ):
  try:
    float( val )
    return False 
  except ValueError:
    return True 
  
def print_version( versionstr ):
  print(
  "Growth Calcualtor - Normal Distribution\
  \n  Version: {}\
  \n  Creator: Ian Gallmeister\
  \n  Source: gitlab.com/ian_g/growth_projection"
  .format( versionstr )
  )
  exit( 0 ) 

if __name__ == '__main__':
  main()
